from django.db import models

# The note model.
class Note(models.Model):
    dest = models.CharField(max_length = 30)            #The destination (receiver) of the message. Uses CharField with maximum length 30 characters.
    origin = models.CharField(max_length = 30)          #The origin (sender) of the message. Uses CharField with a maximum length 30 characters. Not named "from" because from is a python reserved keyword.
    title = models.CharField(max_length = 30)           #The title of the message. Uses CharField with maximum of 30 characters.
    message = models.TextField()                        #The content of the message. Uses TextField so that TextArea widget is used for input.