from django.urls import path, include
from .views import index, xml, json

#Links to paths present on lab_2 views file.
urlpatterns = [
    path("", index, name="index"),
    path("xml", xml, name="xml"),
    path("json", json, name="json")
]