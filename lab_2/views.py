from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note

# index displays all note objects as a table in html.
def index(response):
    notes = Note.objects.all()
    context = {"notes" : notes}
    return render(response, "lab2.html", context)

# xml displays all note objects in an xml form. It can be accessed through the /xml
def xml(response):
    notes = Note.objects.all()
    data = serializers.serialize("xml", notes)
    return HttpResponse(data, content_type="application/xml")

# json displays all note objects in a json form. It can be accessed through the /json
def json(response):
    notes = Note.objects.all()
    data = serializers.serialize("json", notes)
    return HttpResponse(data, content_type="application/json")