from django.contrib import admin
from .models import Note

# Register Note model to admin.
admin.site.register(Note)