from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

# The main page of lab-3 app. Displays all the friends list we have from our Lab 1.
# Needs admin login to enter.
@login_required(login_url="/admin/login")
def index(response):
    friendList = Friend.objects.all()               

    context = {                                     
        "friends" : friendList
    }
    return render(response, "lab3_index.html", context)

# This page contains the form to add a new Friend to the friends list in Lab 1.
# Needs admin login to enter
@login_required(login_url="/admin/login")
def add_friend(response):
    
    # Define the form from the imported FriendForm
    form = FriendForm(response.POST or None)

    # Handle if it's POST request, get the data entered in the form, save it to the database.
    if response.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("/lab-3")

    # Handle if it's GET request, show form in the HTML page.
    context = {
        "form" : form
    }
    
    return render(response, "lab3_form.html", context)