from django.urls import path, include
from .views import index, add_friend

#The following urls lead to the specified links
urlpatterns = [
    path("", index, name="index"),
    path("add", add_friend, name="add")
]