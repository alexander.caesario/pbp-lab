from django import forms
from lab_1.models import Friend

#Create new form model, using ModelForm
class FriendForm(forms.ModelForm):
    class Meta:
        #Create form based on the Friend model, using the fields
        model = Friend
        fields = [
            "name",
            "npm",
            "dob",
        ]