import 'package:flutter/material.dart';

class Category {
  final String id;
  final String title;
  final Color color;

  const Category({
    @required this.id,
    @required this.title,
    this.color = Colors.orange,
  });
}

class Newsfeed {
  final String title;
  final String snippet;
  final String link;

  const Newsfeed({
    @required this.title,
    @required this.snippet,
    @required this.link,
  });
}
