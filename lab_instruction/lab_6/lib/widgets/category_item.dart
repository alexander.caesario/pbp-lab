import 'package:flutter/material.dart';

import '../screens/category_meals_screen.dart';

class CategoryItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  CategoryItem(this.id, this.title, this.color);

  void selectCategory(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      CategoryMealsScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.button,
          textAlign: TextAlign.center,
        ),
        decoration: BoxDecoration(
          color: Colors.amber[50],
          border: Border.all(
            color: Colors.brown[900],
          ),
          borderRadius: BorderRadius.circular(5),
        ),
      ),
    );
  }
}

class NewsfeedItem extends StatelessWidget {
  final String title;
  final String snippet;
  final String link;

  NewsfeedItem(this.title, this.snippet, this.link);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
          child: Column(
            children: [
              Padding(
                  child: Align(
                      child: Text(title,
                          style: TextStyle(fontWeight: FontWeight.w800),
                          textAlign: TextAlign.left),
                      alignment: Alignment.topLeft),
                  padding: EdgeInsets.symmetric(vertical: 7.0)),
              Padding(
                  child: Text(snippet),
                  padding: EdgeInsets.symmetric(vertical: 4.0)),
            ],
          ),
          padding: EdgeInsets.all(20.0)),
      decoration: BoxDecoration(
          color: Color.fromRGBO(253, 248, 238, 1),
          border: Border.all(color: Color.fromRGBO(82, 49, 21, 1), width: 2.0),
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.25),
                offset: Offset(0, 4),
                blurRadius: 4)
          ]),
      width: double.infinity,
      margin: EdgeInsets.all(10.0),
    );
  }
}
