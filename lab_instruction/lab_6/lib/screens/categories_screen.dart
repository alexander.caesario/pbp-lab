import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/category_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new SingleChildScrollView(
          child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          HelloTitle(),
          HomePageTitle(),
          ProjectView(),
          // Expanded(child: SizedBox(height: 200, child: ProjectView())),
          TaskTitle(),
          // Expanded(child: SizedBox(height: 200, child: TaskView())),
          TaskView(),
          NewsfeedTitle(),
          NewsfeedView(),
        ],
      )),
    );
  }
}

class HelloTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        child: Text("Hello, whenwe177!",
            style: TextStyle(
                color: Colors.brown,
                fontFamily: "MADESunflower",
                fontSize: 48)),
        padding: EdgeInsets.all(10));
  }
}

class HomePageTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Text("Recent projects",
          style: TextStyle(
              color: Colors.brown, fontFamily: "MADESunflower", fontSize: 36)),
    );
  }
}

class TaskTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Text("Recent Tasks",
          style: TextStyle(
              color: Colors.brown, fontFamily: "MADESunflower", fontSize: 36)),
    );
  }
}

class NewsfeedTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Text("Newsfeed",
          style: TextStyle(
              color: Colors.brown, fontFamily: "MADESunflower", fontSize: 48)),
    );
  }
}

class ProjectView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      padding: const EdgeInsets.all(25),
      children: DUMMY_CATEGORIES
          .map(
            (catData) => CategoryItem(
              catData.id,
              catData.title,
              catData.color,
            ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
      physics: new NeverScrollableScrollPhysics(),
    );
  }
}

class TaskView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      shrinkWrap: true,
      padding: const EdgeInsets.all(25),
      children: DUMMY_TASKS
          .map(
            (catData) => CategoryItem(
              catData.id,
              catData.title,
              catData.color,
            ),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
      physics: new NeverScrollableScrollPhysics(),
    );
  }
}

class NewsfeedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: DUMMY_NEWS
          .map((news) => NewsfeedItem(news.title, news.snippet, news.link))
          .toList(),
    );
  }
}
