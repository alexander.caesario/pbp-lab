from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(response):
    notes = Note.objects.all()

    context = {
        "notes" : notes
    }

    return render(response, "lab4_index.html", context)

def note_list(response):
    notes = Note.objects.all()

    context = {
        "notes" : notes
    }

    return render(response, "lab4_note_list.html", context)

def add_note(response):
    form = NoteForm(response.POST or None)

    if (response.method == "POST"):
        if form.is_valid():
            form.save()
            return redirect("/lab-4")

    context = {
        "form" : form
    }

    return render(response, "lab4_form.html", context)