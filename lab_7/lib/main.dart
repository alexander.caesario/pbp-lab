import 'package:flutter/material.dart';
import "package:google_fonts/google_fonts.dart";

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flight Search',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.brown,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String text = "Search for flights";
  final originController = TextEditingController();
  final destinationController = TextEditingController();

  void changeText(String text) {
    setState(() {
      String origin = originController.text;
      String destination = destinationController.text;

      if (origin == "" && destination == "") {
        showDialog(
            context: context,
            builder: (_) => const AlertDialog(
                title: Text("Empty Field Required"),
                content:
                    Text("Origin and Destination airport cannot be empty.")),
            barrierDismissible: true);
        return;
      }

      if (origin == "") {
        showDialog(
            context: context,
            builder: (_) => const AlertDialog(
                title: Text("Empty Field Required"),
                content: Text("Origin airport cannot be empty.")),
            barrierDismissible: true);
        return;
      }

      if (destination == "") {
        showDialog(
            context: context,
            builder: (_) => const AlertDialog(
                title: Text("Empty Field Required"),
                content: Text("Origin airport cannot be empty.")),
            barrierDismissible: true);
        return;
      }

      this.text =
          "Results found for flights between ${originController.text} and ${destinationController.text} :";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Flights')),
        body: Container(
            child: Column(
          children: [
            MarginatedContainer(
              Center(
                child: Text("Search your flights in one click.",
                    style: GoogleFonts.rubik(
                      color: const Color.fromRGBO(207, 154, 128, 1),
                      fontSize: 30.0,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center),
              ),
            ),
            MarginatedContainer(
              InsertOriginDestination(
                  "From",
                  Icon(
                    Icons.flight_takeoff,
                    color: Colors.brown[600],
                  ),
                  originController),
            ),
            MarginatedContainer(
              InsertOriginDestination(
                  "To",
                  Icon(
                    Icons.flight_land,
                    color: Colors.brown[600],
                  ),
                  destinationController),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  child: Text(
                    "See schedule".toUpperCase(),
                    style:
                        GoogleFonts.ubuntu(fontSize: 12.0, letterSpacing: 1.1),
                  ),
                  onPressed: () => changeText(text),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0))),
                  ),
                ),
              ),
            ),
            Text(
              text,
              style: GoogleFonts.rubik(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  color: const Color.fromRGBO(191, 124, 57, 1)),
            ),
          ],
        )));
  }
}

class MarginatedContainer extends StatelessWidget {
  final Widget child;

  MarginatedContainer(this.child);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(5.0),
        child: this.child);
  }
}

class InsertOriginDestination extends StatefulWidget {
  final String label;
  final Icon icon;
  final TextEditingController controller;

  InsertOriginDestination(this.label, this.icon, this.controller);

  @override
  _InsertOriginDestinationState createState() =>
      _InsertOriginDestinationState();
}

class _InsertOriginDestinationState extends State<InsertOriginDestination> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      decoration: InputDecoration(
          labelText: widget.label,
          prefixIcon: widget.icon,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50.0),
              borderSide: const BorderSide(
                width: 2.5,
              )),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(50.0),
              borderSide: const BorderSide(
                  color: Color.fromRGBO(90, 122, 0, 1), width: 1.1))),
      style: GoogleFonts.ubuntu(
        color: Colors.brown[800],
      ),
    );
  }
}
