from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = "Alexander Caesario Bangun"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 10, 11)  # Date object, birthdate of myself
npm = 2006519851  # NPM, big integer object


def index(request):
    #Response dictionary, to be passed as parameters to the template
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    #Calculates the age, based on the current year (datetime.now())
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()  # Friends.objects.all() takes all the object of type Friend from the database
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
