from django.db import models
from django.utils import timezone

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here
class Friend(models.Model):
    name = models.CharField(max_length=50)          #Name is of String type, because contains characters, set to maximum 50 chars
    npm = models.BigIntegerField(default=0)         #NPM is BigInteger, because a normal Integer field cannot contain it (Only 32 digits)
    dob = models.DateField(blank=True)              #Date of Birth is of DOB field
    
