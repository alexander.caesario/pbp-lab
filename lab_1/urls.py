from django.urls import path
from .views import index, friend_list

#Paths to index and friend_list
urlpatterns = [
    path('', index, name='index'),
    path('friends', friend_list, name="friends")
]
