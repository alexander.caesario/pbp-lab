##### 1. What are the differences between JSON and XML?

|No | JSON | XML | 
----- | ---- | ---- |
| 1. | JSON is short of "JavaScript Object Notation". It is based off JavaScript. | XML is short of "eXtensible Markup Language". It is based off SGML. |
| 2. | JSON represents data in a similar way to JavaScript Objects (with key and value pairs) | XML represents data enclosed in tags. |
| 3. | JSON supports only text and numbers | XML can support numbers, texts, images, charts and even graphs. |
| 4. | JSON is less secure | XML is more secure |
| 5. | Values from JSON is easy to query | Returning values from XML is more difficult. |

#### 2. What are the differences between XML and HTML?
| No | XML | HTML |
| ---- | ---- | ---- |
| 1. | XML is used to transfer data from the database. | HTML is used to display data on the client-side. |
| 2. | Tags on XML are defined based on the users' needs. | Tags on HTML are already predefined. |
| 3. | XML is case sensitive. | HTML is case sensitive. |
| 4. | XML preserves white spaces. | HTML does not preserve white spaces. |

###### References:
1. [Differences between JSON and XML - GeekForGeeks](https://www.geeksforgeeks.org/difference-between-json-and-xml/)
2. [JSON vs XML: What's the difference? - Guru99](https://www.guru99.com/json-vs-xml-difference.html)
3. [HTML vs XML: Difference Between HTML and XML - upGrad blog](https://www.upgrad.com/blog/html-vs-xml/)
